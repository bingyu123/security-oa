export interface DBCommonType {
  // 只读 任意的key : 字符串
  readonly [key: string]: string | number | boolean;
}
