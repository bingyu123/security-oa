export const enum HTTPCode {
  SUCCESS = 200,
  BAD_REQUEST = 400,
  UNAUTHORIZATION = 401,
  SERVER_ERROR = 500,
}
export const enum RequestMethod {
  GET = 'GET',
  POST = 'POST',
  DELETE = 'DELETE',
  PUT = 'PUT',
}
