import { Context } from 'koa';

type CallBackType = (args: Context) => any;
export interface RouterConfigType {
  url: string;
  method: 'GET' | 'POST' | 'DELETE' | 'PUT';
  callback: CallBackType | CallBackType[];
}
