import koa from './app';
import KosStatic from 'koa-static';
import KoaBodyPaser from 'koa-bodyparser';
import path from 'path';
import koaRouter from './router/index';
import cors from 'koa2-cors';
koa.use(cors()); // 一定放在最上面 跨域
koa.use(KosStatic(path.join(__dirname, './public'))); // 静态资源 public
koa.use(KosStatic(path.join(__dirname, './upload'))); // 静态资源 upload
koa.use(KoaBodyPaser()); // ctx.body.xxx // post参数

// koa.use(
//   cors({
//     origin: function (ctx: Context) {
//       //设置允许来自指定域名请求
//       // if (ctx.url === '/data.json') {
//       //     return '*'; // 允许来自所有域名请求
//       // }
//       // return 'http://localhost:8080'; //只允许http://localhost:8080这个域名的请求
//       return "*";
//     },
//     maxAge: 5, // 指定本次预检请求的有效期，单位为秒。
//     credentials: true, //是否允许发送Cookie
//     allowMethods: ["GET", "POST", "PUT", "DELETE", "OPTIONS"], //设置所允许的HTTP请求方法
//     allowHeaders: ["Content-Type", "Authorization", "Accept"], //设置服务器支持的所有头信息字段
//     exposeHeaders: ["WWW-Authenticate", "Server-Authorization"], //设置获取其他自定义字段
//   })
// );

// koa.use((ctx)=>{
//   ctx.body = {
//     code:200,
//     msg: 'hello'
//   }
// })

// 所有的路由信息 获取到了
// koaRouter.allowedMethods() : 报错状态码  400
koa.use(koaRouter.routes()).use(koaRouter.allowedMethods()); // 路由

koa.listen(4001, () => {
  // 后端端口 4001 localhost:4001
  console.log('server is running.... port is 4001');
});
