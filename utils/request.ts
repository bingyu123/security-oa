import request from 'request';

export async function getDNSSearch(): Promise<string> {
  return await new Promise((resolve, reject) => {
    request.post(
      'https://coding.tools/cn/nslookup',
      {
        form: { queryStr: 'baidu.com', querytype: 'A', dnsserver: '8.8.8.8' },
      },
      function (err, res) {
        if (err) {
          reject('dns error');
        }
        resolve(res.body);
      }
    );
  });
}
// cdn检测
export async function getCDNDisCover(
  url: string
): Promise<{ code: number; data: Array<{ ip: string; location: string }> }> {
  return await new Promise((resolve, reject) => {
    request.get(
      `https://myssl.com/api/v1/tools/cdn_check?domain=${url}`,
      function (err, res) {
        if (err) {
          reject('dns error');
        }
        if (typeof res.body === 'string') {
          resolve(JSON.parse(res.body));
        } else {
          resolve(res.body);
        }
      }
    );
  });
}
