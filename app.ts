// 配置相关
// 数据库的增删改查 封装的方法
// ctx : koa 接口 ctx.query 查询 ctx.delete : 删除

import Koa from 'koa';
import { insert, query, update, deletes, select } from './database/dbUtils';
// app : koa的实例
const app = new Koa();
// 增删改查 :

// typeorm
// 全局都可以使用
app.context.key = 'ice';
app.context.db = {
  insert,
  query,
  update,
  deletes,
  select,
};

// ctx.key : ice

export default app;
