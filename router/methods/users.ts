import { Context } from "koa";

export const getRoles = async function (ctx: Context) {
  ctx.body = {
    code: 200,
    data: {
      roles: "admin",
    },
  };
};

