// url : 对应每一个方法
// /home    GET    -->  methods / home
// /message POST   -->  methods / message
import {
  getHomeList,
  updateList,
  deleteById,
  selectByWhere,
} from './methods/home';
import { getRoles } from './methods/users';
import message from './methods/message';
import { RouterConfigType } from '../types/router';
import collectMessage from './modules/collect-message';
import dtvdConfig from './modules/vulnerability-scan';

const routerConfig: RouterConfigType[] = [
  ...collectMessage,
  ...dtvdConfig,
  {
    url: '/home',
    method: 'GET',
    callback: getHomeList,
  },
  {
    url: '/dns/',
    method: 'GET',
    callback: selectByWhere,
  },
  {
    url: '/updateList',
    method: 'GET',
    callback: updateList,
  },
  {
    url: '/deleteById',
    method: 'GET',
    callback: deleteById,
  },
  {
    url: '/user',
    method: 'GET',
    callback: getRoles,
  },
  {
    url: '/message',
    method: 'GET',
    callback: message,
  },
];

export default routerConfig;
