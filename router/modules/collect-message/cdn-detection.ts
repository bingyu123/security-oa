import { Context } from 'koa';
// import axios from 'axios';
// import { RequestMethod } from '../../../types/http';
import { getCDNDisCover } from '../../../utils/request';
import { load } from 'cheerio';

// const { query } = require("../../db/dbUtils");
export async function getCDNList(ctx: Context) {
  // const res = await ctx.app.context.db.insert('home', { title: '水浒传' });
  // 封装一个async await 错误的处理
  const res = await getCDNDisCover('baidu.com');
  console.log(res);
  // const $ = load(res);
  // const result = $('body').text().split('<script')[0];
  ctx.body = {
    code: 200,
    msg: res,
  };
}
export async function updateList(ctx: Context): Promise<void> {
  // 对Promise应用的一个典型代表
  // 1. 函数返回一个Promise实例
  // 2. 使用async 修饰函数 await 后面跟着Promise实例
  // 3. 以同步的方式就能获取结果
  // const result = await query("select * from home"); // 返回值 : promise实例
  // ctx.insert
  const res = await ctx.app.context.db.update(
    'home',
    { title: '巴拉巴拉小魔仙' },
    { id: 4 }
  );
  ctx.body = {
    code: 200,
    title: 'home',
    msg: res,
  };
  // ctx.body 需要放在同步的环境中
}

export async function deleteById(ctx: Context): Promise<void> {
  const res = await ctx.app.context.db.deletes('home', { id: 4 });
  ctx.body = {
    code: 200,
    title: 'home',
    msg: res,
  };
  // ctx.body 需要放在同步的环境中
}
export async function selectByWhere(ctx: Context): Promise<void> {
  const res = await ctx.app.context.db.select('home', { title: '宋江' }, [
    'id',
    'title',
  ]);
  ctx.body = {
    code: 200,
    title: 'home',
    msg: res,
  };
}
