import { RouterConfigType } from '../../../types/router';
import { getCDNList } from './cdn-detection';
import { getDNSList } from './dns';

const collectMessage: RouterConfigType[] = [
  {
    url: '/dns/list',
    method: 'GET',
    callback: getDNSList,
  },
  {
    url: '/cdn/query',
    method: 'GET',
    callback: getCDNList,
  },
];

export default collectMessage;
