import Router from '@koa/router';
import { RouterConfigType } from '../types/router';
import routerConfig from './router-config';
const koaRouter = new Router({ prefix: '/api' });

// router.get router.post
// routerConfig: 路由的配置文件
routerConfig.forEach((item: RouterConfigType) => {
  // koaRouter.post((ctx)=>{ctx.body = []})
  // @ts-expect-error
  const cb = koaRouter[item.method.toLowerCase()]; // toLowerCase() 小写
  if (Array.isArray(item.callback)) {
    cb.call(koaRouter, item.url, ...item.callback);
  } else {
    cb.call(koaRouter, item.url, item.callback.bind(koaRouter));
  }
});

export default koaRouter;
